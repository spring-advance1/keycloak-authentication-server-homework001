package com.example.webfluxdemo.model.request

import com.example.webfluxdemo.model.entity.CloudInstance
import kotlin.random.Random

data class CloudInstanceRequest(
    val instanceName: String,
    val operatingSystemId: Long,
    val userClientId : String

){
    fun toEntity(): CloudInstance {

        return CloudInstance(
            instanceName = instanceName,
            operatingSystemId = operatingSystemId,
            userClientId = userClientId,
            publicIpAddress = "${Random.nextInt(0,256)}.${Random.nextInt(0,256)}.${Random.nextInt(0,256)}.${Random.nextInt(0,256)}"
        )
    }
}