package com.example.webfluxdemo.model

import java.util.*

data class AppUser(
    val id: String,
    val name: String,
    val email: String,
    val image: String,
    val createdDate: Date,
    val lastModified: Date
)