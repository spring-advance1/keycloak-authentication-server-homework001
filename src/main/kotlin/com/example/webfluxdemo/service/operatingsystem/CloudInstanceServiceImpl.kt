package com.example.webfluxdemo.service.operatingsystem

import com.example.webfluxdemo.config.UserClient
import com.example.webfluxdemo.model.AppUser
import com.example.webfluxdemo.model.dto.CloudInstanceDto
import com.example.webfluxdemo.model.entity.CloudInstance
import com.example.webfluxdemo.model.request.CloudInstanceRequest
import com.example.webfluxdemo.repository.CloudInstanceRepository
import com.example.webfluxdemo.repository.OperatingSystemRepository
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class CloudInstanceServiceImpl(
    val cloudInstanceRepository: CloudInstanceRepository,
    val operatingSystemRepository: OperatingSystemRepository,
    @Qualifier("UserClient") val userClient: WebClient

) : CloudInstanceService {
    override fun create(cloudInstanceRequest: CloudInstanceRequest): Mono<CloudInstanceDto> {
        return cloudInstanceRepository
            .save(cloudInstanceRequest.toEntity())
            .map { res -> res.toDto() }
    }

        override fun findAll(): Flux<CloudInstanceDto> {
        val cloudInstanceFlux = cloudInstanceRepository
            .findAll()

        val osFlux = cloudInstanceFlux
            .flatMap {
                operatingSystemRepository.findById(it.operatingSystemId)
            }

        // To store for all user id
        val userIdFlux:Flux<String> = cloudInstanceFlux.map{
            it.userClientId
        }
        // Find user by specific id
        fun fetchUserById(id: String): Mono<AppUser> = userClient.get()
            .uri("/api/v1/users/{id}", id)
            .retrieve()
            .bodyToMono(AppUser::class.java)


       // The term user for zip operating system with cloud and the store it in cloudOsFlux

        val cloudOsFlux = cloudInstanceFlux.zipWith(osFlux)
            .map {
                val cloud = it.t1
                val myos = it.t2
                val cloudResponse = cloud.toDto()
                cloudResponse.operatingSystem = myos.toDto()
                cloudResponse
            }
        // This use for the zip package and other api or other instance
        return cloudOsFlux.zipWith(userIdFlux)
            .flatMap {
                val myCloud = it.t1
                val userId = it.t2
                val user = fetchUserById(userId)
                    .log()

//                val cloudResponse.appUser = user
                Mono.just(myCloud).zipWith(user)
                    .map { resultTup ->
                            val cloud = resultTup.t1
                            val userResult = resultTup.t2
                            cloud.appUser= userResult
                        cloud
                    }

            }
    }



}